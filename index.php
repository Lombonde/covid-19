<?php
ob_start();
session_start();

require __DIR__."/vendor/autoload.php";

use CoffeeCode\Router\Router;

$router = new Router(ROOT_PATH);

$router->namespace("source\App");

$router->group(null);
$router->get("/", "Web:home", "web.home");
$router->get("/sobre", "Web:about", "web.about");
$router->get("/perguntas-frequentes", "Web:faq", "web.faq");
$router->get("/contactos", "Web:contact");
$router->get("/dashboard", "Web:dashboarDetails");

/**
 * Auth
*/ 
$router->group("painel-admin");
$router->get("/", "Admin:home", "admin.home");
$router->get("/login", "Auth:login", "auth.login");
$router->get("/logoff", "Admin:logoff", "admin.logoff");
$router->get("/casos-no-pais", "Admin:cases", "admin.cases");
/**
 * PERGUNTAS E RESPSOTAS FAQ
*/
$router->get("/perguntas-frequentes", "ControllerFaq:faq", "controllerFaq.faq");
$router->post("/perguntas-frequentes-store", "ControllerFaq:store", "controllerFaq.store");
$router->post("/perguntas-frequentes-update", "ControllerFaq:update", "controllerFaq.update");
$router->post("/perguntas-frequentes-delete", "ControllerFaq:delete", "controllerFaq.delete");

/**
 * UPLOAD DE FICHEIROS FAQ
*/
$router->get("/upload-ficheiros", "ControllerFile:files", "controllerFile.files");
$router->post("/upload-ficheiros-store", "ControllerFile:store", "controllerFile.store");
$router->post("/upload-ficheiros-update", "ControllerFile:update", "controllerFile.update");
$router->post("/upload-ficheiros-delete", "ControllerFile:delete", "controllerFile.delete");

/**
 * Auth
*/
$router->post("/login", "Auth:loginF", "auth.loginf");
$router->post("/casos-no-pais", "AdminRequest:case", "admin-request.store-case");

$router->group("ops");
$router->get("/{errcode}", "Web:error", "web.error");

$router->dispatch();

if($router->error()){
    $router->redirect("web.error", ["errcode" => $router->error()]);
}

ob_end_flush();