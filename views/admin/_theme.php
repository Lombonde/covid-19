<!DOCTYPE html>
<html>
<!-- Mirrored from adminlte.io/themes/AdminLTE/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 14:15:03 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= ($head ?? "COVID-19") ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= asset("bower_components/bootstrap/dist/css/bootstrap.min.css", "admin", "admin"); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= asset("bower_components/font-awesome/css/font-awesome.min.css", "admin"); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= asset("bower_components/Ionicons/css/ionicons.min.css", "admin"); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= asset("dist/css/AdminLTE.min.css", "admin"); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= asset("dist/css/skins/_all-skins.min.css", "admin"); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= asset("bower_components/morris.js/morris.css", "admin"); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= asset("bower_components/jvectormap/jquery-jvectormap.css", "admin"); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= asset("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css", "admin"); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= asset("bower_components/bootstrap-daterangepicker/daterangepicker.css", "admin"); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css", "admin"); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js", "admin"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js", "admin"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="../../../fonts.googleapis.com/css5518.css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        </a>

        <div class="navbar-custom-menu">
            <a type="submit" style="margin-top: 7px; margin-right: 3px;" class="btn btn-primary" href="<?= $router->route('admin.logoff')?>">
              <i class="fa fa-power-off"></i>
              Sair
            </a>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">>
          </div>
          <div class="pull-left info">
            <p>Admin</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">Menu Principal</li>
          <li class="active">
            <a href="<?= $router->route("admin.home") ?>">
              <i class="fa fa-dashboard"></i> <span>Estatísticas</span>
            </a>
          </li>

          <li class="active">
            <a href="<?= $router->route("admin.cases") ?>">
              <i class="fa fa-check-circle-o"></i> <span>Apurados</span>
            </a>
          </li>

          <li class="active">
            <a href="<?= $router->route("admin.faq") ?>">
              <i class="fa fa-question-circle-o"></i> <span>Perguntas</span>
            </a>
          </li>



        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>



    <!--Secção das Páginas Dinámicas -->
    <main class="main_content">
      <?= $v->section("content"); ?>
    </main>


    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.13
      </div>
      <strong>Copyright &copy; 2020 <a href="https://infosi.gov.ao/">INFOSI</a>.</strong> Todos os Direitos Reservados

      <!-- Control Sidebar -->



      <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
  </div>
  </footer>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="<?= asset("bower_components/jquery/dist/jquery.min.js", "admin"); ?>"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= asset("bower_components/jquery-ui/jquery-ui.min.js", "admin"); ?>"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= asset("bower_components/bootstrap/dist/js/bootstrap.min.js", "admin"); ?>"></script>
  <!-- Morris.js charts -->
  <script src="<?= asset("bower_components/raphael/raphael.min.js", "admin"); ?>"></script>
  <script src="<?= asset("bower_components/morris.js/morris.min.js", "admin"); ?>"></script>
  <!-- Sparkline -->
  <script src="<?= asset("bower_components/jquery-sparkline/dist/jquery.sparkline.min.js", "admin"); ?>"></script>
  <!-- jvectormap -->
  <script src="<?= asset("plugins/jvectormap/jquery-jvectormap-1.2.2.min.js", "admin"); ?>"></script>
  <script src="<?= asset("plugins/jvectormap/jquery-jvectormap-world-mill-en.js", "admin"); ?>"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= asset("bower_components/jquery-knob/dist/jquery.knob.min.js", "admin"); ?>"></script>
  <!-- daterangepicker -->
  <script src="<?= asset("bower_components/moment/min/moment.min.js", "admin"); ?>"></script>
  <script src="<?= asset("bower_components/bootstrap-daterangepicker/daterangepicker.js", "admin"); ?>"></script>
  <!-- datepicker -->
  <script src="<?= asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js", "admin"); ?>"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="<?= asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js", "admin"); ?>"></script>
  <!-- Slimscroll -->
  <script src="<?= asset("bower_components/jquery-slimscroll/jquery.slimscroll.min.js", "admin"); ?>"></script>
  <!-- FastClick -->
  <script src="<?= asset("bower_components/fastclick/lib/fastclick.js", "admin"); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?= asset("dist/js/adminlte.min.js", "admin"); ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= asset("dist/js/pages/dashboard.js", "admin"); ?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= asset("dist/js/demo.js", "admin"); ?>"></script>

  <?= $v->section("scripts"); ?>
</body>

</html>