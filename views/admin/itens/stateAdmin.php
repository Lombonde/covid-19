
                        <?php foreach ($casos as $caso) : ?>
                            <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-primary">
                                <div class="inner">
                                <h3><?=(totalCasos($caso->id) ?? 0)?><sup style="font-size: 20px"></sup></h3>

                                <p><?=$caso->tipo_caso?></p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                                </div>
                            </div>
                            </div>

                        <?php endforeach ?>
