<div class="modal fade" id="modalAdicionar<?= $provincia->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar Casos: <?= $provincia->provincia ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?= $router->route("admin-request.store-case") ?>" method="POST">
                        <div class="box-body">
                            <h4>Resultados atuais</h4>
                            <input type="hidden" name="provincia_id" value="<?=$provincia->id?>">
                            <input type="hidden" name="operacao" value="Adicionar">
                            <div class="row">
                                <?php foreach($casos as $caso): ?>
                                    <div class="form-group input-group-sm col-md-3">
                                    <label><?= substr($caso->tipo_caso, 0, 11)?></label>
                                    <input type="number" disabled class="form-control" value="<?= (casosProvincia($provincia->id, $caso->id) ?? 0) ?>">
                                </div>
                                <?php endforeach ?>
                            </div>
                            <hr>
                            <h4>Novos resultados</h4>
                            <div class="row">
                                <?php foreach($casos as $caso): ?>
                                    <div class="form-group input-group-sm col-md-3">
                                    <label for="exampleInputEmail1"><?= substr($caso->tipo_caso, 0, 11)?></label>
                                    <input type="hidden" name="caso_id[]" value="<?=$caso->id?>">
                                    <input type="number" class="form-control" name="caso_value[]" value="0">
                                </div>
                                <?php endforeach ?>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modalRemover<?= $provincia->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remover Casos: <?= $provincia->provincia ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="box box-danger">
                    <!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?= $router->route("admin-request.store-case") ?>" method="POST">
                        <div class="box-body">
                            <h4>Resultados atuais</h4>
                            <input type="hidden" name="provincia_id" value="<?=$provincia->id?>">
                            <input type="hidden" name="operacao" value="Subtrair">
                            <div class="row">
                                <?php foreach($casos as $caso): ?>
                                    <div class="form-group input-group-sm col-md-3">
                                    <label><?= substr($caso->tipo_caso, 0, 11)?></label>
                                    <input type="number" disabled class="form-control" value="<?= (casosProvincia($provincia->id, $caso->id) ?? 0) ?>">
                                </div>
                                <?php endforeach ?>
                            </div>
                            <hr>
                            <h4>Novos resultados</h4>
                            <div class="row">
                                <?php foreach($casos as $caso): ?>
                                    <div class="form-group input-group-sm col-md-3">
                                    <label for="exampleInputEmail1"><?= substr($caso->tipo_caso, 0, 11)?></label>
                                    <input type="hidden" name="caso_id[]" value="<?=$caso->id?>">
                                    <input type="number" class="form-control" name="caso_value[]" value="0">
                                </div>
                                <?php endforeach ?>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-danger">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>