<div class="modal fade" id="addModalFaq" tabindex="-1" role="dialog" aria-labelledby="addModalFaq" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalFaqLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
          
         
            <form action="<?=$router->route("controllerFile.store") ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputEmail">Titulo</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Titulo">
                </div>

                <div class="form-group">
                  <label for="file">Ficheiro</label>
                  <input type="file" class="form-control" id="file" name="file" >
                </div>
              </div>

              <!-- /.box-body -->
              <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button class="btn btn-primary">Salvar</button>
        </div>
            </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>
