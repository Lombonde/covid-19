<div class="modal fade" id="editFaqModal<?=$pergunta->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header box box-primary">
            <h4 style="" >Editar Caso</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form role="form" action="<?=$router->route("controllerFaq.update") ?>" method="POST">
          <div class="modal-body">
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->

            
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputEmail">Pergunta</label>
                  <input type="text" class="form-control" value="<?=$pergunta->pergunta?>" id="pergunta" name="pergunta" placeholder="Pergunta">
                </div>

                <div class="box-body pad">
                  
                <textarea class="textarea" name="resposta" id="resposta" placeholder="Place some text here"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                              <?=$pergunta->resposta?>
                              </textarea>
                <input type="hidden"  value="<?=$pergunta->id?>" id="id" name="id">
                
                </div>
              </div>
              <!-- /.box-body -->
         
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Actualizar</button>
          </div>
        </div>
        </form>
      </div>
    </div>