<div class="modal fade" id="editFileModal<?=$file->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header box box-primary">
            <h4 style="" >Editar Ficheiros</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form role="form" action="<?=$router->route("controllerFile.update") ?>" method="POST">
          <div class="modal-body">
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->

            
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputEmail">Titulo</label>
                  <input type="text" class="form-control" value="<?=$file->nome?>" id="name" name="name" >
                </div>

                <div class="box-body pad">
                  
                <input type="file" class="form-control" id="file" name="file">

                <input type="hidden"  value="<?=$file->id?>" id="id" name="id">
                
                </div>
              </div>
              <!-- /.box-body -->
         
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Actualizar</button>
          </div>
        </div>
        </form>
      </div>
    </div>