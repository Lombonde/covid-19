<div class="modal fade" id="caseModal<?=$case->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header box box-primary">
            <h4 style="" >Editar Caso</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="box box-primary">
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form">
                <div class="box-body">
                  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Caso:</label> <label for=""><?=$case->tipo_caso?></label>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Quantidade</label>
                    <input type="email" class="form-control" value="<?=$case->quantidade?>" placeholder="Quantidade">
                  </div>
                </div>
                <!-- /.box-body -->
              </form>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-primary">Actualizar</button>
          </div>
        </div>
      </div>
    </div>