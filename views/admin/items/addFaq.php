<div class="modal fade" id="addModalFaq" tabindex="-1" role="dialog" aria-labelledby="addModalFaq" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalFaqLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" action="<?=$router->route("controllerFaq.store") ?>" method="POST">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputEmail">Pergunta</label>
                  <input type="text" class="form-control" id="pergunta" name="pergunta" placeholder="Pergunta">
                </div>

                <div class="box-body pad">
                  
                    <textarea class="textarea" name="resposta" id="resposta" placeholder="Descrição"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                
                </div>
              </div>
              <!-- /.box-body -->
              <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
            </form>
          </div>
        </div>
        
      </div>
    </div>
  </div>
