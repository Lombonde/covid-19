<div class="modal fade" id="deleteFaqModal<?=$pergunta->id?>" tabindex="-1" role="dialog" aria-labelledby="deleteFaqLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header box box-primary">
            <h4 style="" >Excluir Perguntas</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form role="form" action="<?=$router->route("controllerFaq.delete")?>" method="POST">
          <div class="modal-body">
          <div class="box box">
            <!-- /.box-header -->
            <!-- form start -->

            
              <div class="box-body">
                
                <div class="box-body pad">
                  Tem certeza que pretendes excluir?
            
                <input type="hidden"  value="<?=$pergunta->id?>" id="id" name="id">
                
                </div>
              </div>
              <!-- /.box-body -->
         
          </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-danger">Excluir</button>
          </div>
        </div>
        </form>
      </div>
    </div>