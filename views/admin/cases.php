<?php $v->layout("_theme"); ?>

<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">ESTADO EPIDEMIOLÓGICO DE CASOS COVID-19</h3>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <?= flash(); ?>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Província</th>
                  <?php foreach ($casos as $caso) : ?>
                    <th><?= $caso->tipo_caso ?></th>
                  <?php endforeach; ?>
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($provincias as $provincia) : ?>
                  <tr>
                    <td><?= $provincia->provincia ?></td>
                    <?php foreach ($casos as $caso) : ?>
                      <td><?= (casosProvincia($provincia->id, $caso->id) ?? 0) ?></td>
                    <?php endforeach; ?>
                    <td><?= (casosProvincia($provincia->id) ?? 0) ?></td>
                    <td class="text-center">
                      <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalAdicionar<?= $provincia->id ?>">Adicionar</a>
                      <a href="" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalRemover<?= $provincia->id ?>">Remover</a>
                    </td>
                  </tr>
                  <?= $v->insert("itens/modals", ["provincia" => $provincia, "casos" => $casos]); ?>
                <?php endforeach; ?>
              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>