<?php $v->layout("_theme");?>

      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        UPLOAD DE FICHEIROS DO COVID-19
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Início</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">LISTA</h3>
            <div class="text-right m-0">
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#addModalFaq">Adicionar</a>
            </div>
          </div>
          <?php $v->insert("items/addFile");?>


          <!-- /.box-header -->
            <!-- /.box-header -->
            <?= flash(); ?>
            <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nome do Ficheiro</th>
                  <th>Caminho</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                
                  <?php if ($files): foreach ($files as $file): ?>
                      <tr>
                      <td><?=$file->nome?></td>
                      <td><?=$file->caminho?></td>
                      <td>
                        </td>
                    </tr>
                    <?php $v->insert("items/deleteFile", ["files" => $file]);?>

                    <?php $v->insert("items/editFile", ["files" => $file]);?>
                  <?php endforeach;else:
                        ?>
                         <h4><div class="alert alert-warning alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i>  Não existem ficheiros cadastradas!</h4>
                            </div>
                         </h4>
                    <?php
                  endif; ?>
                </tbody>
              
                </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->