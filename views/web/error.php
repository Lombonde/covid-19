<?php $v->layout("_theme"); ?>

<div class="container ">
<div class="space-100"></div>
    <div class="center-title error-title">
        <h1><?=$error?></h1>
    </div>
    <div class="text-center">
        <a class="btn btn-primary" href="<?=$router->route("web.home")?>">Ir para página Inicial</a>
    </div>
    <div class="space-100"></div>
</div>