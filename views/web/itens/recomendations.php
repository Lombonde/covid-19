<div class="row align-items-center">
    <div class="col-lg-3 margin-b-30">
        <h3 class="textm strong colorf">
            MEDIDAS E RECOMENDAÇÕES COVID-19
        </h3>
    </div>
    <div class="col-lg-8 margin-b-30">
        <div class="row">
            <div class="col-md-6">
                <ul class="list">
                    <li>
                        <a href="<?= asset("arq_pdf/Decreto Legislativo Presidencial.pdf") ?>" target="_blank" style="color: black;">
                            <i class="ion-ios-arrow-thin-right"></i>
                            Decreto Legislativo Presidencial Provisório n.º 1/20 de 18 de Março
                        </a>
                    </li>
                    <li>
                        <a href="<?= asset("arq_pdf/2020 DRI 031 (28) OK.pdf.pdf.pdf") ?>" target="_blank" style="color: black;">
                            <i class="ion-ios-arrow-thin-right"></i>
                            Decreto Legislativo Presidencial Provisório n.º 1/20 de 18 de Março(Versão DR)
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list">

                    <li>
                        <a href="<?= asset("arq_pdf/QUARENTENA DOMICILIAR  REV  18 mar 2020 versao 2.pdf") ?>" target="_blank" style="color: black;">
                            <i class="ion-ios-arrow-thin-right"></i>
                            Medidas extraordinária para quarentena domiciliar
                        </a>
                    </li>
                    <li>
                        <a href="<?= asset("arq_pdf/COMUNICADO COVID 19  ALERTA POP GERAL_versão2.pdf") ?>" target="_blank" style="color: black;">
                            <i class="ion-ios-arrow-thin-right"></i>
                            Comunicado Covid19
                        </a>
                    </li>
                    <li>
                        <a href="<?= asset("arq_pdf/COVID-19 PREVENÇÃO mg.doc") ?>" target="_blank" style="color: black;">
                            <i class="ion-ios-arrow-thin-right"></i>
                            Prevenção Covid19
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
<div class="space-60"></div>