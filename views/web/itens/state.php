    <!-- class cor cradiente    skin-bg-->
    <section class="lastsection container box effect7" style="margin-top: 30px; margin-bottom: 30px;">
        <div class="col-lg-12 ">
            <div class="row">
                <div class="col-md-12" class="">
                    <div class="row">

                        <div class="col-md-2 col-s-12">
                            <h2 class="f-size3 text-black strong">ESTADO DO<br> COVID-19 EM ANGOLA </h2>
                        </div>

                        <?php foreach ($casos as $caso) : ?>
                            <div class="col-md-2 col-s-12 text-center">
                                <span class="big-number text-black"><?=(totalCasos($caso->id) ?? 0)?></span><br>
                                <span class="f-size2 text-black"><?=$caso->tipo_caso?></span>
                            </div>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>
    </section>