    <!-- class cor cradiente    skin-bg-->
    <section class="lastsection container box effect7" style="margin-top: 30px; margin-bottom: 30px;">
        <div class="col-lg-12 ">
            <div class="row">
                <div class="col-md-12" class="">
                    <div class="row">

                        <div class="col-md-2 col-s-12">
                            <h2 class="f-size3 text-black strong">ESTADO DO<br> COVID-19 EM ANGOLA </h2>
                        </div>

                        <?php foreach ($casos as $caso) : ?>
                            <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                <h3><?=(totalCasos($caso->id) ?? 0)?><sup style="font-size: 20px"></sup></h3>

                                <p><?=$caso->tipo_caso?></p>
                                </div>
                                <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                                </div>
                            </div>
                            </div>

                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>
    </section>