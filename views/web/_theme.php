<!DOCTYPE html>
<html lang="pt">

<!-- Mirrored from bootstraplovers.com/bootstrap4/saas-v2.5/html/index-slider.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jul 2019 08:22:58 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$head?></title>

    <!-- plugins -->
    <link href="<?= asset("css/plugins/bundle.css") ?>" rel="stylesheet">
    <!--main css file-->
    <link href="<?= asset("css/style.css") ?>" rel="stylesheet">
    <link rel="icon" type="image/png" href="<?= asset("images/favicon.png") ?>">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"
            charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js" charset="utf-8"></script>
    <script src="<?= asset("_js/jquery.mapael.js") ?>"></script>
    <script src="<?= asset("_js/maps/angola.js") ?>"></script>
</head>

<body>

    <!--/preloader-->

    <!--back to top-->
    <a href="#" class="scrollToTop"><i class="ion-ios-arrow-up"></i></a>
    <!--back to top end-->
    <div class="topheader">
        <img src="<?= asset("images/bandeira_angola.jpg") ?>" class="img-responsive " width="100%" alt="">
    </div>
    <!-- Fixed navbar -->


    <nav class="navbar navbar-expand-lg navbar-light navbar-default ">

        <div class="clearfix"></div>
        <div class="container-fluid">

            <!-- Start Top Search -->
            <div class="top-search clearfix">

                <div class="input-group">

                    <input type="text" class="form-control" placeholder="pesquisar">
                    <span class="input-group-addon close-search"><i class="ion-android-close"></i></span>
                </div>

            </div>
            <!-- End Top Search -->
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand  strong" href="index.html">
                <img src="<?= asset("images/govlogotop.png") ?>" class="logoresp" width="30% " style="margin-left: 200px;" alt="" class="logo-scroll">
            </a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">

            </div>
        </div>
    </nav>

    <?= $v->section("content") ?>

    <style>
        #vertical {
            height: 80px;
            border-left: 0.1px solid #fff;
        }
    </style>


    <footer class=" footer ">
        <div class="space-10 "></div>
        <div class="container ">
            <div class="row vertical-align-child " style="">
                <div class="col-md-3 margin-b-30 ">
                    <div class="margin-b-10 ">
                        <a href="# ">
                            <img src="<?= asset("images/govlogo.png") ?>" class="logobottom" width="60% " alt=" ">
                        </a>
                    </div>
                    <p>
                        República de Angola
                    </p>
                </div>
                <div class="col-md-4 margin-b-30 ">
                    <h4>CONTACTOS TELEFÓNICOS ÚTEIS</h4>
                    <ul class="list-unstyled ">
                        <li>
                            <a href="#">+244 937 503 349</a>
                            <p>SOS CORONAVÍRUS.</p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 margin-b-30 ">
                    <h4>CONTACTOS DIGITAIS ÚTEIS</h4>
                    <div class="container">
                        <div class="row vertical-align-child">
                            <div class="col-md-8 margin-b-30">
                                <ul class="list social ">
                                    <li class="list-inline-item"><a href="https://www.facebook.com/COVID19GovAo" target="_blank"><i style="font-size:30px" class="ion-social-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="https://www.instagram.com/COVID19GovAo" target="_blank"><i style="font-size:30px" class="ion-social-instagram"></i></a></li>

                                    <li class="list-inline-item"><a href="https://www.Twitter.com/COVID19GovAo" target="_blank"><i style="font-size:30px" class="ion-social-twitter"></i></a></li>

                                    <li class="list-inline-item"><a href="https://www.Youtube.com/COVID19GovAo" target="_blank"><i style="font-size:30px" class="ion-social-youtube"></i></a></li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <!-- jQuery plugins-->
   


</body>

</html>