<?php $v->layout("_theme"); ?>

<style type="text/css">
    /* Specific mapael css class are below
         * 'mapael' class is added by plugin
        */
    .mapcontainer{
        max-width: 500px;
        margin-left: 320px;
    }
    .mapael .map {
        position: relative;
    }

    .mapael .mapTooltip {
        position: absolute;
        background-color: #fff;
        moz-opacity: 0.90;
        opacity: 0.90;
        filter: alpha(opacity=90);
        border-radius: 10px;
        padding: 10px;
        z-index: 1000;
        max-width: 200px;
        display: none;
        color: #343434;
    }
</style>

<script type="text/javascript">
    $(function() {
        $(".mapcontainer").mapael({
            map: {
                name: "angola",
                defaultArea: {
                    attrs: {
                        stroke: "#fff",
                        "stroke-width": 1
                    }
                }
            },
            legend: {
                area: {
                    title: "Áreas de Risco",
                    slices: [{
                            max: 100,
                            attrs: {
                                fill: "#1a527b"
                            },
                            label: "Menos de 100 casos"
                        },
                        {
                            min: 1000,
                            max: 9999,
                            attrs: {
                                fill: "#ff4c24"
                            },
                            label: "Mais de 1000 casos"
                        },
                        {
                            min: 10000,
                            max: 999999,
                            attrs: {
                                fill: "#ff370a"
                            },
                            label: "Mais de 10000 casos"
                        },
                        {
                            min: 1000000,
                            attrs: {
                                fill: "#f02c00"
                            },
                            label: "Mais de 1000000 casos"
                        }
                    ]
                }
            },
            areas: {

                "AO-BGO": {
                    "value": "<?=casosProvincia("1")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Bengo<\/span><br \/> Confirmados: <?=casosProvincia("1", "1")?><br \/>Suspeitos: <?=casosProvincia("1", "2")?> <br \/> Recuperados: <?=casosProvincia("1", "3")?><br \/>Óbitos: <?=casosProvincia("1", "4")?>"
                    },
                    "text": {
                        "content": "Bengo"
                    }
                },
                "AO-BGU": {
                    "value": "<?=casosProvincia("2")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Benguela<\/span><br \/> Confirmados: <?=casosProvincia("2", "1")?><br \/>Suspeitos: <?=casosProvincia("2", "2")?> <br \/> Recuperados: <?=casosProvincia("2", "3")?><br \/>Óbitos: <?=casosProvincia("2", "4")?>"
                    },
                    "text": {
                        "content": "Benguela"
                    }
                },

                "AO-BIE": {
                    "value": "<?=casosProvincia("3")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Bié<\/span><br \/> Confirmados: <?=casosProvincia("3", "1")?><br \/>Suspeitos: <?=casosProvincia("3", "2")?> <br \/> Recuperados: <?=casosProvincia("3", "3")?><br \/>Óbitos: <?=casosProvincia("2", "4")?>"
                    },
                    "text": {
                        "content": "Bié"
                    }
                },
                "AO-CAB": {
                    "value": "<?=casosProvincia("4")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Cabinda<\/span><br \/> Confirmados: <?=casosProvincia("4", "1")?><br \/>Suspeitos: <?=casosProvincia("4", "2")?> <br \/> Recuperados: <?=casosProvincia("4", "3")?><br \/>Óbitos: <?=casosProvincia("4", "4")?>"
                    },
                    "text": {
                        "content": "Cabinda"
                    }
                },
                "AO-CCU": {
                    "value": "<?=casosProvincia("5")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Cuando-Cubango<\/span><br \/> Confirmados: <?=casosProvincia("5", "1")?><br \/>Suspeitos: <?=casosProvincia("5", "2")?> <br \/> Recuperados: <?=casosProvincia("5", "3")?><br \/>Óbitos: <?=casosProvincia("5", "4")?>"
                    },
                    "text": {
                        "content": "Cuando-Cubango"
                    }
                },
                "AO-CNO": {
                    "value": "<?=casosProvincia("7")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Cuanza Norte<\/span><br \/> Confirmados: <?=casosProvincia("7", "1")?><br \/>Suspeitos: <?=casosProvincia("7", "2")?> <br \/> Recuperados: <?=casosProvincia("7", "3")?><br \/>Óbitos: <?=casosProvincia("7", "4")?>"
                    },
                    "text": {
                        "content": "Cuanza Norte"
                    }
                },
                "AO-CUS": {
                    "value": "<?=casosProvincia("6")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Cuanza Sul<\/span><br \/> Confirmados: <?=casosProvincia("6", "1")?><br \/>Suspeitos: <?=casosProvincia("6", "2")?> <br \/> Recuperados: <?=casosProvincia("6", "3")?><br \/>Óbitos: <?=casosProvincia("6", "4")?>"
                    },
                    "text": {
                        "content": "Cuanza Sul"
                    }
                },
                "AO-CNN": {
                    "value": "<?=casosProvincia("8")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Cunene<\/span><br \/> Confirmados: <?=casosProvincia("8", "1")?><br \/>Suspeitos: <?=casosProvincia("8", "2")?> <br \/> Recuperados: <?=casosProvincia("8", "3")?><br \/>Óbitos: <?=casosProvincia("8", "4")?>"
                    },
                    "text": {
                        "content": "Cunene"
                    }
                },
                "AO-HUA": {
                    "value": "<?=casosProvincia("9")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Huambo<\/span><br \/> Confirmados: <?=casosProvincia("9", "1")?><br \/>Suspeitos: <?=casosProvincia("9", "2")?> <br \/> Recuperados: <?=casosProvincia("9", "3")?><br \/>Óbitos: <?=casosProvincia("9", "4")?>"
                    },
                    "text": {
                        "content": "Huambo"
                    }
                },
                "AO-HUI": {
                    "value": "<?=casosProvincia("10")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Huíla<\/span><br \/> Confirmados: <?=casosProvincia("10", "1")?><br \/>Suspeitos: <?=casosProvincia("10", "2")?> <br \/> Recuperados: <?=casosProvincia("10", "3")?><br \/>Óbitos: <?=casosProvincia("10", "4")?>"
                    },
                    "text": {
                        "content": "Huíla"
                    }
                },
                "AO-LUA": {
                    "value": "<?=casosProvincia("11")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Luanda<\/span><br \/> Confirmados: <?=casosProvincia("11", "1")?><br \/>Suspeitos: <?=casosProvincia("11", "2")?> <br \/> Recuperados: <?=casosProvincia("11", "3")?><br \/>Óbitos: <?=casosProvincia("11", "4")?>"
                    },
                    "text": {
                        "position": "left",
                        "content": "Luanda"
                    }
                },
                "AO-LNO": {
                    "value": "<?=casosProvincia("12")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Lunda Norte<\/span><br \/> Confirmados: <?=casosProvincia("12", "1")?><br \/>Suspeitos: <?=casosProvincia("12", "2")?> <br \/> Recuperados: <?=casosProvincia("12", "3")?><br \/>Óbitos: <?=casosProvincia("12", "4")?>"
                    },
                    "text": {
                        "content": "Lunda Norte"
                    }
                },
                "AO-LSU": {
                    "value": "<?=casosProvincia("13")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Lunda Sul<\/span><br \/> Confirmados: <?=casosProvincia("13", "1")?><br \/>Suspeitos: <?=casosProvincia("13", "2")?> <br \/> Recuperados: <?=casosProvincia("13", "3")?><br \/>Óbitos: <?=casosProvincia("13", "4")?>"
                    },
                    "text": {
                        "content": "Lunda Sul"
                    }
                },
                "AO-MAL": {
                    "value": "<?=casosProvincia("14")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Malanje<\/span><br \/> Confirmados: <?=casosProvincia("14", "1")?><br \/>Suspeitos: <?=casosProvincia("14", "2")?> <br \/> Recuperados: <?=casosProvincia("14", "3")?><br \/>Óbitos: <?=casosProvincia("14", "4")?>"
                    },
                    "text": {
                        "content": "Malanje"
                    }
                },
                "AO-MOX": {
                    "value": "<?=casosProvincia("15")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Moxico<\/span><br \/> Confirmados: <?=casosProvincia("15", "1")?><br \/>Suspeitos: <?=casosProvincia("15", "2")?> <br \/> Recuperados: <?=casosProvincia("15", "3")?><br \/>Óbitos: <?=casosProvincia("15", "4")?>"
                    },
                    "text": {
                        "content": "Moxico"
                    }
                },
                "AO-NAM": {
                    "value": "<?=casosProvincia("16")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Namibe<\/span><br \/> Confirmados: <?=casosProvincia("16", "1")?><br \/>Suspeitos: <?=casosProvincia("16", "2")?> <br \/> Recuperados: <?=casosProvincia("16", "3")?><br \/>Óbitos: <?=casosProvincia("16", "4")?>"
                    },
                    "text": {
                        "content": "Namibe"
                    }
                },
                "AO-UIG": {
                    "value": "<?=casosProvincia("17")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Uíge<\/span><br \/> Confirmados: <?=casosProvincia("17", "1")?><br \/>Suspeitos: <?=casosProvincia("17", "2")?> <br \/> Recuperados: <?=casosProvincia("17", "3")?><br \/>Óbitos: <?=casosProvincia("17", "4")?>"
                    },
                    "text": {
                        "content": "Uíge"
                    }
                },
                "AO-ZAI": {
                    "value": "<?=casosProvincia("18")?>",
                    "href": "#",
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">Zaire<\/span><br \/> Confirmados: <?=casosProvincia("18", "1")?><br \/>Suspeitos: <?=casosProvincia("18", "2")?> <br \/> Recuperados: <?=casosProvincia("18", "3")?><br \/>Óbitos: <?=casosProvincia("18", "4")?>"
                    },
                    "text": {
                        "content": "Zaire"
                    }
                }
            }
        });
    });
</script>

<div class="container">
    <div class="center-title">
        <h2>ACOMPANHAMENTOS DE CASOS POR COVID-19.</h2>
    </div>
    <div class="row">
        <div class="mapcontainer">
            <div class="map">
                <span>Alternative content for the map</span>
            </div>
            <div class="areaLegend">
                <span>Alternative content for the map</span>
            </div>
        </div>
    </div>
</div>