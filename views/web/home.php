<?php $v->layout("_theme"); ?>

<!-- ESTADO DO COVID-19 EM ANGOLA -->
<?= $v->insert("itens/state", ["casos" => $casos]) ?>

<div class="cta-skin">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4 margin-b-20 skin-bg3  ">
                <div class="feature-box-center text-center text-uppercase">
                    <a href="tel:111">
                        <div class="align-self-center ">
                            <h3 class="text-white strong" style="font-size:48px; margin-bottom: -10px;">
                                <img src="<?= asset("img/cisp-img.png") ?>" class="text-white" alt="" width="32%" title="">
                            </h3>
                            <p class=""> <strong style="font-size: 28px;"><img src="<?= asset("img/ligar.png") ?>" class="text-white" alt="" width="32" height="32" title="">111</strong>
                                CONTACTOS DE EMERGÊNCIA
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <!--/col-->
            <div class="col-md-6 col-lg-4  margin-b-10">
                <div class="feature-box-center text-center">
                    <iframe width="100%" height="250" src="https://www.youtube.com/embed/IU2ls6d7q1g" frameborder="0" allow="accelerometer; 
                        autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <!--/col-->
            <div class="col-md-6 col-lg-4 margin-b-20 skin-bg4">
                <div class="feature-box-center text-uppercase text-center">

                    <a href="<?=$router->route("web.faq")?>">
                        <div class="align-self-center">
                            <h3 class="text-white strong">FAQS</h3>
                            <p>Aceda aqui à lista de Perguntas mais Frequentes, regularmente atualizada, com
                                resposta das entidades oficiais.</p>
                        </div>

                    </a>
                </div>
            </div>

        </div>
    </div>
</div>

<section class="showcase-section">
    <div class="space-40"></div>
    <div class="container">
    <!-- INFORME DIÁRIO -->
    <?= $v->insert("itens/dayinfo") ?>

    <!-- MEDIDAS E RECOMENDAÇÕES COVID-19 -->
    <?= $v->insert("itens/recomendations") ?>
    </div>
</section>