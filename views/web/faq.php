<?php $v->layout("_theme"); ?>

<div class="container">
    <div class="center-title">
        <h2>PERGUNTAS E RESPOSTAS</h2>
    </div>
    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
            <div id="accordion" class="card-accrodions" role="tablist" aria-multiselectable="true">

                
                <?php if ($faqs) : ?>
                    <?php foreach ($faqs as $faq) : ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $faq->id ?>" aria-expanded="true" aria-controls="collapseOne">
                                        <?= $faq->pergunta ?>
                                    </a>
                                </h5>
                            </div>

                            <div id="collapse<?= $faq->id ?>" class="collapse" role="tabpanel" aria-labelledby="heading<?= $faq->id ?>">
                                <div class="card-body">
                                    <p><?= $faq->resposta ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php endif; ?>

            </div>
        </div>
        <div class="space-90"></div>
    </div>
</div>