<?php

namespace Source\App;

use Source\Models\CasosPorProvincia;
use Source\Models\Cases;
use Source\Models\Pergunta;

class ControllerFaq extends Controller{

    public function __construct($router) {
        parent::__construct($router, "views/admin");
    }
   
    public function faq():void
    {
        $head = "Perguntas Frequentes";

        $perguntas = (new Pergunta())->find()->fetch(true);

        echo $this->view->render("faq", [
            "perguntas" => $perguntas
        ]);
    }

    public function store($data){

        $pergunta=new Pergunta();
         
        if($pergunta->insertFaq($data) ){

            flash("success", "Dados inseridos com sucesso", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }
        else{
            flash("danger", "Ocorreu um erro ao inserir", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }
    } 
    
    public function update($data){

        $pergunta=new Pergunta();

        if($pergunta->updateFaq($data) ){

            flash("warning", "Dados editados com sucesso", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }
        else{
            flash("danger", "Ocorreu um erro ao actualizar", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }
       
    } 

    public function delete($data){

        $pergunta=new Pergunta();
       
        if($pergunta->deleteFaq($data) ){
            flash("danger", "Dados excluidos com sucesso", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }
        else{
            flash("danger", "Ocorreu um erro ao excluir", "fa-thumbs-up");
            $this->router->redirect("controllerFaq.faq");
        }

    } 

    

}