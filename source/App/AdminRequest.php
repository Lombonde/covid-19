<?php

namespace Source\App;

use Source\Models\CasosPorProvincia;
use Source\Models\Cases;
use Source\Models\Pergunta;

class AdminRequest extends Controller{

    public function __construct($router) {
        parent::__construct($router, "views/admin");
    }

    public function case($data):void
    {
        $cases_id = $data["caso_id"];
        $cases_value = $data["caso_value"];

        if(!$cases_id || !$cases_value){
            echo "Alguma coisa está em falta";
        }

        for($i=0; $i < count($cases_id); $i++){
            $new_case = (new CasosPorProvincia());
            $new_case->provincia_id = $data["provincia_id"];
            $new_case->caso_id = $cases_id[$i];
            $new_case->quantidade = ($data["operacao"] == "Adicionar") ? abs($cases_value[$i]) : subtrair_casos($data["provincia_id"], $cases_id[$i] ,abs($cases_value[$i])) ;
            // echo subtrair_casos($data["provincia_id"], $cases_id[$i] ,abs($cases_value[$i]));
            // die();
            // $new_case->quantidade = abs($cases_value[$i]);
            $new_case->operacao = $data["operacao"];
            $new_case->save();
        }

        flash("success", "Dados actualizados com sucesso", "thumbs-up");
        $this->router->redirect("admin.cases");
        
    }

}