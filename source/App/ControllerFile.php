<?php

namespace Source\App;

use Source\Models\CasosPorProvincia;
use Source\Models\Cases;
use Source\Models\Files;
use Source\App\File;
use Source\App\Uploader;

class ControllerFile extends Controller{

    public function __construct($router) {
        parent::__construct($router, "views/admin");
    }
   
    public function files():void
    {
        $head = "Upload de Ficheiros";

        $file = (new Files())->find()->fetch(true);

        echo $this->view->render("files", [
            "files" => $file
        ]);
    }
    
    public function store($data){
        
        /*
        $file = new File("uploads", "files");

        if ($_FILES) {
            try {
                $upload = $file->upload($_FILES[$data["file"]], $_POST[$data["file"]]);
                echo "<p><a href='{$upload}' target='_blank'>@CoffeeCode</a></p>";
            } catch (Exception $e) {
                echo "<p>(!) {$e->getMessage()}</p>";
            }
        }*/
        $file = new \CoffeeCode\Uploader\File("uploads", "files");

        if ($_FILES) {
            try {
                $upload = $file->upload($_FILES['file'], $_POST['name']);
                echo "<p><a href='{$upload}' target='_blank'>@CoffeeCode</a></p>";

                $fileUp = $_FILES['file'];
                $name = $_POST['name'];
                
                $fileBD=new Files();

                $fileBD->insertFiles($name, $upload);

                if($fileBD > 0){
            
                    flash("success", "Ficheiro inserido com sucesso", "fa-thumbs-up");
                    $this->router->redirect("controllerFile.files");
    
                }
                else{
                    flash("danger", "Ocorreu um erro", "fa-thumbs-up");
                    $this->router->redirect("controllerFile.files");
                }
                
            
            } catch (Exception $e) {
                echo "<p>(!) {$e->getMessage()}</p>";
            }
        }
       
    } 
    
    public function update($data){

        $file=new Files();

        if($file->updatefiles($data) ){

            flash("warning", "Dados editados com sucesso", "fa-thumbs-up");
            $this->router->redirect("controllerFiles.files");
        }
        else{
            flash("danger", "Ocorreu um erro ao actualizar", "fa-thumbs-up");
            $this->router->redirect("controllerFiles.files");
        }
       
    } 

    public function deleteFile($data){

        $file=new Files();
       
        if($file->deleteFiles($data) ){
            flash("danger", "Dados excluidos com sucesso", "fa-thumbs-up");
            $this->router->redirect("controllerFile.file");
        }
        else{
            flash("danger", "Ocorreu um erro ao excluir", "fa-thumbs-up");
            $this->router->redirect("controllerFile.file");
        }

    } 

    

}