<?php

namespace Source\App;

use Source\Models\User;

class Auth extends Controller{

    public function __construct($router) {
        parent::__construct($router, "views/admin");

        if (!empty($_SESSION["user"])) {
            $this->router->redirect("admin.home");
        }
    }

    public function login()
    {
        echo $this->view->render("login", [
            "head" => "Faça Login para continuar"
        ]);
    }

    public function loginF($data)
    {
        $email = filter_var($data["email"], FILTER_VALIDATE_EMAIL);
        $passwd = filter_var($data["passwd"], FILTER_DEFAULT);

        if(!$email){
            flash("warning", "Email inválido!");
            $this->router->redirect("auth.login");
        }

        $user = (new User())->findByEmail($email);
        if(!$user || !passwd_verify($passwd, $user->passwd)){
            flash("warning", "Dados de usuários não conferem!");
            $this->router->redirect("auth.login");
        }

        $_SESSION["user"] = $user->id;
        $this->router->redirect("admin.home");
    }

}