<?php

namespace Source\App;

use Source\Models\Caso;
use Source\Models\Pergunta;

class Web extends Controller{

    public function __construct($router) {
        parent::__construct($router);
    }

    public function home()
    {
        $casos = (new Caso())->find()->fetch(true);

        echo $this->view->render("home", [
            "head" => "Página Inicial - COVID-19",
            "casos" => $casos
        ]);
    }


    public function faq()
    {
        $faqs = (new Pergunta())->find()->fetch();
        echo $this->view->render("faq", [
            "head" => "Perguntas Frequentes - COVID-19",
            "faqs" => $faqs
        ]);
    }


    public function dashboarDetails()
    {
        $casos = (new Caso())->find()->fetch(true);

        echo $this->view->render("dashboard", [
            "head" => "Detalhes - COVID-19",
            "casos" => $casos
        ]);


        
    }

    public function error($data)
    {
        echo $this->view->render("error", [
            "head" => "Erro {$data["errcode"]} - COVID-19",
            "error" => $data["errcode"]
        ]);
    }

}