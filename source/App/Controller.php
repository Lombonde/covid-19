<?php

namespace  Source\App;

use CoffeeCode\Router\Router;
use League\Plates\Engine;

abstract class Controller
{

    /** @var Engine */
    protected $view;

    /** @var Router */
    protected $router;

    public function __construct($router, $view = "views/web")
    {
        $this->router = $router;
        $this->view = Engine::create(dirname(__DIR__, 2) . "/" . $view, "php");
        $this->view->addData(["router" => $this->router]);
    }

    public function ajaxResponse(string $param, array $values): string
    {
        return json_encode([$param => $values]);
    }
}
