<?php

namespace Source\App;

use Source\Models\Caso;
use Source\Models\Pergunta;
use Source\Models\Provincia;
use Source\Models\CasosPorProvincia;
use Source\Models\User;

class Admin extends Controller{

    public function __construct($router) {
        parent::__construct($router, "views/admin");
        
        if(empty($_SESSION["user"]) || !$this->user = (new User())->findById($_SESSION["user"])){
            
            unset($_SESSION["user"]);

            flash("danger", "Acesso negado. Favor logue-se");
            $this->router->redirect("auth.login");
        }
    }

    public function home():void
    {
        $provincias = (new Provincia())->find()->fetch(true);
        $casos = (new Caso())->find()->fetch(true);
        echo $this->view->render("dashboard", [
            "provincias" => $provincias,
            "casos" => $casos
        ]);

    }


    public function cases():void
    {
        $head = "Casos no País";
        $provincias = (new Provincia())->find()->fetch(true);
        $casos = (new Caso())->find()->fetch(true);
        echo $this->view->render("cases", [
            "provincias" => $provincias,
            "casos" => $casos
        ]);
    }

    public function error($data):void
    {
        echo "Admin Erro: {$data["errcode"]}";
    }

    public function logoff()
    {
        unset($_SESSION["user"]);

        flash("info", "Você saiu com sucesso, volte logo {$this->user->first_name}");
        $this->router->redirect("auth.login");
    }
}