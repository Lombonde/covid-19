<?php

namespace Source\Models;
use CoffeeCode\DataLayer\DataLayer;

class Pergunta extends DataLayer{

    public function __construct() {
 
        parent::__construct("faq", ["pergunta", "resposta"]);
         
    }

    public function insertFaq($data){

        $this->pergunta = $data["pergunta"];
        $this->resposta = $data["resposta"];
        
        return $this->save();

    }

    public function updateFaq($data){

        $faq = (new Pergunta()) ->findbyId($data["id"]);
        $faq->pergunta = $data["pergunta"];
        $faq->resposta = $data["resposta"];
        return $faq->save();


    }

    public function deleteFaq($data){

       
        $pergunta = (new Pergunta())->findbyId($data["id"]);

        
            $pergunta->destroy();
          
    }

      
    
}