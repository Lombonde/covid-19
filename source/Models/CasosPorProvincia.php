<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class CasosPorProvincia extends DataLayer{

    public function __construct() {
        parent::__construct("casos_provincia", ["provincia_id", "caso_id", "quantidade", "operacao"]);
    }

}