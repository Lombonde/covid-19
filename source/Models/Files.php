<?php

namespace Source\Models;
use CoffeeCode\DataLayer\DataLayer;

class Files extends DataLayer{

    public function __construct() {
 
        parent::__construct("files", ["nome", "caminho"]);
         
    }

    public function insertFiles($name, $file){

        
        $this->nome = $name;
        $this->caminho = $file;
        
        //var_dump($this);
        //die();
        return $this->save();
        
    }

    public function updateFiles($data){

        $files = (new Files())->findbyId($data["id"]);
        $files->nome = $data["nome"];
        $files->caminho = $data["caminho"];
        return $files->save();


    }

    public function deleteFiles($data){
       
        $files = (new Files())->findbyId($data["id"]);

        
        $files->destroy();
    }

      
    
}