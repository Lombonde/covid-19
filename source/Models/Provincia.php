<?php

namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Provincia extends DataLayer{

    public function __construct() {
        parent::__construct("provincias", ["provincia", "abreviatura"], "id", false);
    }

    public function casos()
    {
        return (new Caso())->find("provincia_id = :pi", "pi={$this->id}")->fetch(true);
    }

}