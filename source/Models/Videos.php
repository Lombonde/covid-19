<?php

namespace Source\Models;
use CoffeeCode\DataLayer\DataLayer;

class Videos extends DataLayer{

    public function __construct() {
 
        parent::__construct("faq", ["pergunta", "resposta"]);
         
    }

    public function salvar($data){

        $this->pergunta = $data["pergunta"];
        $this->resposta = $data["resposta"];
        
        return $this->save();

    }
    

}