<?php

namespace Source\Models;
use CoffeeCode\DataLayer\DataLayer;

class Cases extends DataLayer{

    public function __construct() {
        parent::__construct("casos", ["tipo_caso", "quantidade"]);
    }

}