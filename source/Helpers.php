<?php

use Source\Models\CasosPorProvincia;

function asset(string $path, $level = "web", $time = true): string
{
    $file = ROOT_PATH . "/views/{$level}/assets/{$path}";
    $fileOnDir = dirname(__DIR__, 1) . "/views/{$level}/assets/{$path}";

    if ($time && file_exists($fileOnDir)) {
        $file .= "?time=" . filemtime($fileOnDir);
    }
    return $file;
}

function passwd_verify($passwd, $hash){
    return password_verify($passwd, $hash);
}

function flash(string $type = null, string $message = null, $icon = "info"): ?string
{

    if ($type && $message) {
        $_SESSION["flash"] = [
            "type" => $type,
            "message" => $message,
            "icon" => $icon
        ];

        return null;
    }

    if (!empty($_SESSION["flash"]) && $flash = $_SESSION["flash"]) {

        unset($_SESSION["flash"]);
        return "<div class=\"callout callout-{$flash["type"]}\">
                    <p><i class=\"fa fa-{$flash["icon"]}\"></i> {$flash["message"]}</p>
                </div>";
    }

    return null;
}

function casosProvincia($provincia, $caso = null)
{

    if (!$caso) {
        $casos = (new CasosPorProvincia)->find(
            null,
            null,
            "
                (SELECT SUM(quantidade) FROM casos_provincia WHERE provincia_id ={$provincia} AND operacao='Adicionar') AS adicionados,
                (SELECT SUM(quantidade) FROM casos_provincia WHERE provincia_id ={$provincia} AND operacao='Subtrair') AS subtraidos
            "
        )->fetch();

        // var_dump($casos);
        // die();

        if (!$casos) {
            return null;
        }

        $total = ($casos->adicionados - $casos->subtraidos);
        return ($total < 0) ? null : $total;
    }

    $casos = (new CasosPorProvincia)->find(
        null,
        null,
        "
            (SELECT SUM(quantidade) FROM casos_provincia WHERE caso_id ={$caso} AND provincia_id ={$provincia} AND operacao='Adicionar') as adicionados,
            (SELECT SUM(quantidade) FROM casos_provincia WHERE caso_id ={$caso} AND provincia_id ={$provincia} AND operacao='Subtrair') as subtraidos
        "
    )->fetch();

    if (!$casos) {
        return null;
    }

    $total = ($casos->adicionados - $casos->subtraidos);
    return ($total < 0) ? null : $total;
}

function totalCasos($caso)
{
    $casos = (new CasosPorProvincia)->find(
        null,
        null,
        "
                (SELECT SUM(quantidade) FROM casos_provincia WHERE caso_id ={$caso} AND operacao='Adicionar') AS adicionados,
                (SELECT SUM(quantidade) FROM casos_provincia WHERE caso_id ={$caso} AND operacao='Subtrair') AS subtraidos
            "
    )->fetch();

    if (!$casos) {
        return null;
    }

    $total = ($casos->adicionados - $casos->subtraidos);
    return ($total < 0) ? null : $total;
}

function subtrair_casos($provincia, $caso, $valor){
    
    if($valor < 1){
        return $valor;
    }

    $casos = (new CasosPorProvincia)->find(
        null,
        null,
        "
            (SELECT SUM(quantidade) FROM casos_provincia WHERE provincia_id ={$provincia} AND caso_id ={$caso} AND operacao='Adicionar') AS adicionados,
            (SELECT SUM(quantidade) FROM casos_provincia WHERE provincia_id ={$provincia} AND caso_id ={$caso} AND operacao='Subtrair') AS subtraidos
        "
    )->fetch();

    if(!$casos){
        return "0";
    }

    if(($casos->subtraidos + $valor) >= $casos->adicionados){
        return ($casos->adicionados - $casos->subtraidos);
    }
    
    return $valor;
}
